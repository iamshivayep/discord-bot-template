// Setup our environment variables via dotenv
require('dotenv').config()



// Import relevant classes from discord.js
const { Client, Intents, MessageEmbed } = require('discord.js');
const TicTacToe = require("discord-tictactoe")
// Instantiate a new client with some necessary parameters.
const client = new Client(
    { intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES] }
);

// makes the bot respond to a message

client.on('messageCreate', (message) => {
    if (message.content === "-what-my-luck") {
        const rndweapon = ["Unlucky", "Lucky", "SuperLucky", "GodlyLucky", "Luckiest of all (highest luck!)"]
        const randomMessage = rndweapon[Math.floor(Math.random() * rndweapon.length)];
        message.channel.send(randomMessage)
    }
})

client.on('messageCreate', (message) => {
    if (message.content === "-am-i-a-god") {
        message.channel.send("compiling youre-god.exe........")
        message.channel.send("GBC compiler now compiling file69 at current time.....")
        message.channel.send("found compiler data at current time.....")
        const successrate = ["Failed to compile, GBC Compiler Found error in file69 at line69", "Successfully compiled file69 (youre-good.exe) at current time."]
        const successrate_sender = successrate[Math.floor(Math.random() * successrate.length)]
        message.channel.send(successrate_sender)
    }
})

client.on('messageCreate', (message) => {
    if (message.content === "-open-sus-box") {
        const user = message.author.id
        message.channel.send("you open a sus box and find.... https://tenor.com/26hp.gif")

        const susboxitems = ["you recived an amogus! ", "you didn't get anything. :(", "you recived a rare weapon."]
        const susboxitems_rater = susboxitems[Math.floor(Math.random() * susboxitems.length)]
        message.channel.send(susboxitems_rater)


    }
})

// help page
client.on('messageCreate', (message) => {
    if (message.content === "-help") {
        const exampleEmbed = new MessageEmbed()
            .setColor('#0099ff')
            .setTitle('Help')
            .setURL('https://discord.js.org/')
            .setAuthor({ name: 'By: The owner, Shiva.' })
            .setDescription('lists all the available commands with descriptions.')
            .addFields(
                { name: '-what-my-luck', value: 'shows your luck!' },
                { name: '-am-i-a-god', value: "i am not going to spoil this one for you, check it out for yourself! :)"},
                { name: "-open-sus-box ", value: "Opens a mysterybox, you can recive something or nothing!"}
                
            )
            .setFooter({ text: 'Note: Bot is still in early stages of development, more commands may be added or removed.' });

        message.channel.send({ embeds: [exampleEmbed] });
    }
})

// Notify progress
client.on('ready', function (e) {
    console.log(`Logged in as ${client.user.tag}!`)
})



//vars


// Authenticate
client.login(process.env.tokensus)
