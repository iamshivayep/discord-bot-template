# Usage
to use this bot, make sure you have your discord bot application setup, copy your token and lets hop right into it!

# How to apply
to apply the code to your bot, open this file with your ide or code editor. 
```
discord-bot-template/.env

```
once there you will see the variable `tokensus=yourtokenhere`. remove the `yourtokenhere` and replace it with your bot token, make sure this file isn't shared with anyone.

# Running
First install [NodeJS](https://nodejs.org/en/) 
once you have got NodeJS up and running, open the /discord-bot-template in the terminal or windows cmd
there run the following command
```
node index.js
```
and you should be up and running by now!

# Contribute
we are always free for contribution, you can contribute to this project and once we verify, your feature will surely be added! show us what you got!
